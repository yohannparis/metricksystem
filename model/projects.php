<?php
// list of projects

$projects['Einstein'] = array(
	'client' => '',
	'project' => 'Einstein',
	'desc' => 'Introducing the world’s first 3D printed book and the Dinner of the&nbsp;Century',
	'tags' => array('Video Production'),
	'title' => 'Einstein',
	'texte' => 'Introducing the world’s first 3D printed book and the Dinner of the&nbsp;Century',
	'artwork' => array(
		array('type' => 'video', 'src' => 'http://metricktv.com/_content/videos/6Einstein_bookvideoc_720Pweb.mov', 'poster' => 'Einstein.jpg')
	)
);

$projects['Aurelle'] = array(
	'client' => 'aurelle.png',
	'project' => 'Aurelle',
	'desc' => 'Aurelle for Walmart. We&nbsp;created the&nbsp;glitter.',
	'tags' => array('Brand Strategy'),
	'title' => 'AURELLE',
	'texte' => 'Creating a new jewellery line for Walmart had significant challenges. An existing chain, a significant
	number of stores, a "we’ve done it this way for years" attitude; to their credit, an open mind for suggestions.
	We asked, what should it be? From name creation, logo development and comprehensive package design, our
	inspiration came from the periodic table of elements.',
	'artwork' => array(
		array('type' => 'image', 'src' => 'AurelleLogo.jpg'),
		array('type' => 'image', 'src' => 'AurellePackaging.jpg'),
	)
);

$projects['Overland'] = array(
	'client' => '',
	'project' => 'Overland',
	'desc' => 'Creating an animated video to explain hard to explain&nbsp;software.',
	'tags' => array('Video Production'),
	'title' => 'Overland',
	'texte' => 'Creating an animated video to explain hard to explain software.',
	'artwork' => array(
		array('type' => 'video', 'src' => 'http://metricktv.com/_content/videos/3Overland_Sphere3d1000pxwide.mov', 'poster' => 'Overland.jpg')
	)
);

$projects['Elte'] = array(
	'client' => 'elte.png',
	'project' => 'Elte',
	'desc' => 'Elte is the second largest home décor retail store in North&nbsp;America.',
	'tags' => array('Advertising','Brand Strategy','Digital','Social Media','Web Design','Content Creation'),
	'title' => 'ELTE',
	'texte' => 'BUILD IT AND THEY WILL COME — Elte is one of the world’s largest home décor stores with six
	acres of beautiful products under one roof. Our audience was already motivated - we set out to inspire
	them starting with famed  photographer David Drebin. We contributed to interior signage systems, outdoor,
	print and digital advertising, web design and video.',
	'artwork' => array(
		array('type' => 'image', 'src' => 'ELTE_Billboard.jpg'),
		array('type' => 'image', 'src' => 'ELTE_Website.jpg'),
		array('type' => 'image', 'src' => 'ELTE_DD1.jpg'),
		array('type' => 'image', 'src' => 'ELTE_DD2.jpg'),
		array('type' => 'image', 'src' => 'ELTE_DD3.jpg')
	)
);

$projects['Subcretary'] = array(
	'client' => 'mrsub.png',
	'project' => 'Subcretary',
	'desc' => 'One of a series of nine spots based on Woody Allen\'s <em>What\'s&nbsp;Up&nbsp;Tiger&nbsp;Lily?</em>',
	'tags' => array('Video Production'),
	'title' => 'Mr.Sub Subcretary',
	'texte' => 'One of a series of nine spots based on Woody Allen\'s <em>What\'s Up Tiger Lily?</em>',
	'artwork' => array(
		array('type' => 'video', 'src' => 'http://metricktv.com/_content/videos/Subcretary_TW.mov', 'poster' => 'MRSUB-Subcretary.jpg')
	)
);

$projects['Gingers'] = array(
	'client' => 'gingers.png',
	'project' => 'Gingers',
	'desc' => 'Ginger’s: One of the finest bathroom stores in all the world, just ask Martha&nbsp;Stewart.',
	'tags' => array('Advertising','Brand Strategy','Digital','Web Design','Content Creation'),
	'title' => 'GINGER\'S',
	'texte' => 'Ginger’s is considered one of the world’s great bathroom stores and a leader in its field
	for product selection and great design. Print magazine, outdoor, interior sign systems and video
		encompassed the branding effort.',
	'artwork' => array(
		array('type' => 'image', 'src' => 'Gingers2.jpg'),
		array('type' => 'image', 'src' => 'Gingers1.jpg'),
		array('type' => 'image', 'src' => 'Gingers_Billboard.jpg'),
		array('type' => 'image', 'src' => 'Gingers_Showerhead.jpg')
	)
);

$projects['HR'] = array(
	'client' => 'harryrosen.png',
	'project' => 'HR',
	'desc' => 'Harry Rosen celebrated their 60th anniversary this&nbsp;year.',
	'tags' => array('Video Production'),
	'title' => 'HARRY ROSEN',
	'texte' => 'Harry Rosen is not only Canada’s premier men’s luxury clothing empire, it’s a leader in men\'s
	retailing throughout the world. The 60th Legacy video is a tribute to their success founded from a simple
	idea in a small clothing store on Toronto’s Parliament Street.',
	'artwork' => array(
		array('type' => 'video', 'src' => 'http://metricktv.com/_content/videos/HR_60th_legacy_V3_720p.mov', 'poster' => 'HarryRosen_videoPoster3.jpg')
	)
);

$projects['EinsteinMuseum'] = array(
	'client' => '',
	'project' => 'EinsteinMuseum',
	'texte' => 'The Einstein Musuem will be the first and only institution to celebrate the life, history and vision of Albert Einstein. Built on the site of the historic Hebrew University in Jerusalem, where Einstein bequeathed his entire personal archive, the museum will be a global attraction dedicated to science and humanitarian&nbsp;ideals.',
	'tags' => array('Content Creation'),
	'title' => 'Einstein Museum',
	'desc' => 'The Einstein Musuem will be the first and only institution to celebrate the life, history and vision of Albert&nbsp;Einstein.',
	'artwork' => array(
		array('type' => 'image', 'src' => 'EinsteinMuseum-1.jpg'),
		array('type' => 'image', 'src' => 'EinsteinMuseum-3.jpg')
	)
);

$projects['SJ'] = array(
	'client' => 'shoeless.png',
	'project' => 'SJ',
	'desc' => 'Shoeless Joe\'s is all about celebrating sports with family and friends from 37 locations throughout&nbsp;Ontario.',
	'tags' => array('Advertising','Digital','Social Media','Content Creation'),
	'title' => 'SHOELESS JOE\'S',
	'texte' => 'Shoeless Joe\'s Sports Grill is a chain of 37 restaurants located in Southern Ontario that
	celebrates sports, food and drinks with fans of all ages. Our goal is to make them the \'Ultimate Sports
	Destination\' through digital advertising and radio.',
	'artwork' => array(
		array('type' => 'video', 'src' => 'http://metricktv.com/_content/videos/SJ%20Score_Radio.mov', 'poster' => 'SJ.jpg'),
		array('type' => 'video', 'src' => 'http://metricktv.com/_content/videos/SJ%20Team%20Marriage_Radio.mov', 'poster' => 'SJ.jpg')
	)
);

$projects['Telus'] = array(
	'client' => 'telus.png',
	'project' => 'Telus',
	'desc' => 'Telus: Finally, a mobile company who cares about its&nbsp;customers.',
	'tags' => array('Video Production'),
	'title' => 'TELUS',
	'texte' => 'Telus is both the most generous corporate charitable donor and the largest employer of adorable
	critters in Canada. So if you have to use a cell phone &ndash; it may as well be Telus.',
	'artwork' => array(
		array('type' => 'video', 'src' => 'http://metricktv.com/_content/videos/CreditLimit_EN_R7b_854.mov', 'poster' => 'Telus_videoPoster.jpg'),
		array('type' => 'video', 'src' => 'http://metricktv.com/_content/videos/Roaming_854%20H264%20FRE%20R2.mov', 'poster' => 'Telus_videoPoster.jpg')
	)
);

$projects['WhosWho'] = array(
	'client' => 'mrsub.png',
	'project' => 'WhosWho',
	'desc' => 'Two friends raised $1500 to start a fresh sandwich shop in the 60\'s, today there are 400&nbsp;Mr.Sub\'s.',
	'tags' => array('Advertising','Digital','Social Media','Web Design','Experiential','Content Creation','Video Production'),
	'title' => 'MR. SUB',
	'texte' => 'What could be more Canadian than the Hinterland’s "Who\'s Who"?. This campaign defined our client as
	the ‘Canadian’ sub chain in a sea to shining sea of sandwich makers. The comprehensive campaign included television,
	radio, interior signage, menu boards, posters and training videos.',
	'artwork' => array(
		array('type' => 'video', 'src' => 'http://metricktv.com/_content/videos/TW_TV_Turkeybacon.mov', 'poster' => 'MRSUBTurkey_videoPoster.jpg')
	)
);

$projects['Shootout'] = array(
	'client' => 'mrsub.png',
	'project' => 'Shootout',
	'desc' => ' Mr. Sub Shootout: A dream of every&nbsp;Canadian.',
	'tags' => array('Advertising','Brand Strategy','Digital','Social Media','Web Design','Experiential','Content Creation','Video Production'),
	'title' => 'SHOOTOUT',
	'texte' => 'Create a national campaign that would reach 3,000,000 Canadians. Build a database targeting men 18-54.
	Develop an on-going event the client could own. <br>The Score: From coast to coast, 12,454 customers purchased
	product from 400 stores. 1,454,000 hits to the website, 44,000 additional Facebook fans, 2,400 in attendance cheering, and
	400 players taking 800 dream shots against former NHL goalies at The Air Canada Centre in Toronto.',
	'artwork' => array(
		array('type' => 'image', 'src' => 'MRSUB_Shootout1.jpg'),
		array('type' => 'image', 'src' => 'MRSUB_Shootout2.jpg'),
		array('type' => 'image', 'src' => 'MRSUB_Shootout3.jpg'),
		array('type' => 'image', 'src' => 'MRSUB_ShootoutWebsite.jpg'),
		array('type' => 'video', 'src' => 'http://metricktv.com/_content/videos/Shootout2012_withAnimation.mov', 'poster' => 'MRSUBShootout_videoPoster.jpg')
	)
);

/*
$projects['Corel'] = array(
	'client' => 'corel.png',
	'project' => 'Corel',
	'desc' => 'In 1989 Corel developed the first Windows based graphics software, today they have 100,000,000&nbsp;users.',
	'tags' => array('Video Production'),
	'title' => 'Corel',
	'texte' => 'A Canadian success story launched twenty-five years ago with WordPerfect, today Corel’s suite of
	software products are used by 100,000,000 worldwide. ',
	'artwork' => array(
		array('type' => 'video', 'src' => 'http://metricktv.com/_content/videos/Designs_720P.mov', 'poster' => 'Corel_Designs_videoPoster.jpg')
	)
);

$projects['JBJ'] = array(
	'client' => 'jbj.png',
	'project' => 'JBJ',
	'desc' => 'Jones Bros: Rebranding a leading jeweller in&nbsp;Illinois.',
	'tags' => array('Advertising','Brand Strategy','Digital','Social Media','Web Design','Experiential','Content Creation'),
	'title' => 'JONES BROS.',
	'texte' => 'We spent a number of months determining what success could look like for a jeweller outside of
	Chicago. We started with their existing customers, determined what touch points were needed to improve their
	average ticket size and frequency of purchase. We then built a new website to anchor an integrated campaign
	and rolled out a coordinated radio, outdoor and digital promotion to attract new customers.',
	'artwork' => array(
		array('type' => 'image', 'src' => 'JBJ_Billboard.jpg'),
		array('type' => 'image', 'src' => 'JBJ_Billboard2.jpg'),
		array('type' => 'image', 'src' => 'JBJ_Billboard3.jpg'),
		array('type' => 'image', 'src' => 'JBJ_Website.jpg')
	)
);

$projects['SJ2'] = array(
	'client' => 'shoeless.png',
	'project' => 'SJ2',
	'desc' => 'Voiced by NFL Films narrator Earl Mann',
	'tags' => array('Video Production'),
	'title' => 'Shoeless Joe’s radio',
	'texte' => '',
	'artwork' => array(
		array('type' => 'video', 'src' => 'http://metricktv.com/_content/videos/1Shoeless Radio-Score Points.mov', 'poster' => 'Shoeless-Radio-Score.jpg')
	)
);

$projects['Crescent'] = array(
	'client' => '',
	'project' => 'Crescent',
	'desc' => 'Chronicling 100 years at an independent boys school in Toronto.',
	'tags' => array('Video Production'),
	'title' => 'Crescent',
	'texte' => '',
	'artwork' => array(
		array('type' => 'video', 'src' => 'http://metricktv.com/_content/videos/2Crescent_MET music1Shortest2015_.mov', 'poster' => 'Crescent.jpg')
	)
);

$projects['TurkeyBacon'] = array(
	'client' => 'mrsub.png',
	'project' => 'TurkeyBacon',
	'desc' => 'A tribute from the iconic Hinterlands Who’s Who for another Canadian icon.',
	'tags' => array('Advertising','Digital','Social Media','Web Design','Experiential','Content Creation','Video Production'),
	'title' => 'MR. SUB',
	'texte' => '',
	'artwork' => array(
		array('type' => 'video', 'src' => 'http://metricktv.com/_content/videos/4TurkeyBacon720P_2400kbps.mov', 'poster' => 'TurkeyBacon.jpg')
	)
);

$projects['HRtrends'] = array(
	'client' => 'harryrosen.png',
	'project' => 'HRtrends',
	'desc' => 'Satorial splendor: a learning tool for the associates at Harry Rosen',
	'tags' => array('Video Production'),
	'title' => 'Harry Rosen trends',
	'texte' => '',
	'artwork' => array(
		array('type' => 'video', 'src' => 'http://metricktv.com/_content/videos/5_03_TE_Heritage Jacket-s.mov', 'poster' => 'Heritage-Jacket.jpg')
	)
);

$projects['Einstein'] = array(
	'client' => '',
	'project' => 'Einstein',
	'desc' => 'Introducing the world’s first 3D book and the Dinner of the Century.',
	'tags' => array('Video Production'),
	'title' => 'Einstein',
	'texte' => '',
	'artwork' => array(
		array('type' => 'video', 'src' => 'http://metricktv.com/_content/videos/6Einstein_bookvideoc_720Pweb.mov', 'poster' => 'Einstein.jpg')
	)
);

$projects['NAC'] = array(
	'client' => 'nac.png',
	'project' => 'NAC',
	'desc' => 'North America\'s only original creative and media planning advertising&nbsp;competition.',
	'tags' => array('Advertising','Brand Strategy','Digital','Social Media','Web Design','Experiential','Content Creation'),
	'title' => 'NATIONAL ADVERTISING CHALLENGE',
	'texte' => 'The National Advertising Awards is North America\'s only original creative awards competition. Now in
	its thirteenth year, advertising teams from across the country compete to produce the best creative work for brands
	including Jell-O, Ziploc, Scotties EnviroCare, Bull\'s Eye BBQ Sauce, Porsche, Royal Bank and Canada’s Walk of Fame.
	Creating a brand voice for the NAC included logo and website design, collateral materials, advertising, creating a
	judging app, and an amazing awards Gala for 600.',
	'artwork' => array(
		array('type' => 'image', 'src' => 'NAA_Illustration.jpg'),
		array('type' => 'image', 'src' => 'NAA_Website2014.jpg'),
		array('type' => 'image', 'src' => 'NAA_JudgingApp2014.jpg'),
		array('type' => 'image', 'src' => 'NAA_Gala1.jpg')
	)
);

$projects['Forte'] = array(
	'client' => 'forte.png',
	'project' => 'Forte',
	'desc' => 'Spectacular bathroom products designed in Toronto and manufactured in Italy.',
	'tags' => array('Brand Strategy'),
	'title' => 'Forte',
	'texte' => 'Forte is a powerhouse in the decorative plumbing and hardware industry with 250
	leading showrooms in the United States and Canada carrying their&nbsp;products.',
	'artwork' => array(
		array('type' => 'image', 'src' => 'Forte_3.jpg'),
		array('type' => 'image', 'src' => 'Forte_2.jpg'),
		array('type' => 'image', 'src' => 'Forte_1.jpg')
	)
);
*/
