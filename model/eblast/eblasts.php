<?php
// list of eblasts

$eblasts['overland'] = array(
	'header' => 'unnamed-6.jpg',
	'video' => array (
		'poster' => '',
		'live' => 'http://metricktv.com/_content/videos/Overland-Sphere3d-Full_Final-H264_web FCON_best_854x480_1100mbps.mov',
		'mp4' => '/model/eblast/overland.mp4',
		'webm' => '/model/eblast/overland.webm'
	),
	'paragraph' => array (
			'Video allows you to convey large amounts of information in a visually arresting way. Take this video we created for Sphere 3D.',
			'They needed to tell people how their technology allowed company personnel to share documents anywhere, anyplace, anytime. But just saying that wasn’t compelling. We needed to show it. So we used the power of video to make it crystal clear.',
			'Once we did, the company increased it’s value from 70 cents a share to 9 dollars a share.',
			'What could we do for you?'
	)
);

$eblasts['telus'] = array(
	'header' => 'unnamed-5.jpg',
	'video' => array (
		'poster' => '',
		'live' => 'http://metricktv.com/_content/videos/ONLINE_POSTPAID ENG_May4thB_YoutubeStream.mov',
		'mp4' => '/model/eblast/telus.mp4',
		'webm' => '/model/eblast/telus.webm'
	),
	'paragraph' => array (
			'Let’s face it, Telus is a big company with a lot of moving parts. And even though they’re a telephone company, they don’t want to keep anyone on hold.',
			'This video we created allowed customers to get the skinny on how to manage their online accounts with ease. WAY better than a long-winded phone tutorial.',
			'It worked so well for Telus they asked us to create an entire series.',
			'Makes us think we could do the same for you?'
	)
);

$eblasts['corel'] = array(
	'header' => 'unnamed-4.jpg',
	'video' => array (
		'poster' => '',
		'live' => 'http://metricktv.com/_content/videos/Patternsv3_854_.mov',
		'mp4' => '/model/eblast/corel.mp4',
		'webm' => '/model/eblast/corel.webm'
	),
	'paragraph' => array (
			'Corel uses complex technology to give users easy-to-use apps. But explaining how they work isn’t exactly the simplest thing in the world.',
			'The video we created walked them through the story in an easy-to-understand way and created a ton of downloads.',
			'Corel was so impressed with the results they asked us to create videos for two other apps.',
			'Think we can do the same for you? You better believe it.'
	)
);

$eblasts['subcretary'] = array(
	'header' => 'unnamed-3.jpg',
	'video' => array (
		'poster' => '',
		'live' => 'http://metricktv.com/_content/videos/Subcretary_TW.mov',
		'mp4' => '/model/eblast/subcretary.mp4',
		'webm' => '/model/eblast/subcretary.webm'
	),
	'paragraph' => array (
			'Mr. Sub needed a cost-effective way of conveying their personality to their customers.',
			'So we put our thinking caps on and created a video campaign that did it in a big way.',
			'The video series was a big hit with customers and created tons of talk value.',
			'Hey, there’s more to videos than you think. Let us show you.'
	)
);

$eblasts['harry60'] = array(
	'header' => 'unnamed-2.jpg',
	'video' => array (
		'poster' => '',
		'live' => 'http://metricktv.com/_content/videos/Harry Rosen No CRM_4min_2200kbps.mov',
		'mp4' => '/model/eblast/harry60.mp4',
		'webm' => '/model/eblast/harry60.webm'
	),
	'paragraph' => array (
			'Harry Rosen turned 60. It’s a huge milestone. But more than that it was an opportunity to deepen their customer’s connection to their brand by making them feel part of their success.',
			'Check out how we used the power of video production to create an emotional response.',
			'The video was well-received to say the least. It lead Harry Rosen to ask us to apply the same thinking to their sales staff videos.',
			'How can we use the power of production to help your brand?'
	)
);

$eblasts['einstein'] = array(
	'header' => 'unnamed-1.jpg',
	'video' => array (
		'poster' => '',
		'live' => 'http://metricktv.com/_content/videos/Einstein_bookvideoc_720Pweb.mov',
		'mp4' => '/model/eblast/einstein.mp4',
		'webm' => '/model/eblast/einstein.webm'
	),
	'paragraph' => array (
			'Canadian Friends of Hebrew University was looking to leverage the 100th Anniversary of Albert Einstein’s Theory of Relativity. So they sought out to create the world’s first 3D printed book as a way to drum up donations toward Hebrew U.',
			'Watch this video to see what allowed them to generate $30 million in donations from a single donor.',
			'This video has since become a compelling sales tool for ongoing donations.',
			'How can we use the power of production to help your cause?'
	)
);

$eblasts['harrytraining'] = array(
	'header' => 'unnamed.jpg',
	'video' => array (
		'poster' => '',
		'live' => 'http://metricktv.com/_content/videos/00_TE_Heritage_Jacket5.mov',
		'mp4' => '/model/eblast/harrytraining.mp4',
		'webm' => '/model/eblast/harrytraining.webm'
	),
	'paragraph' => array (
			'Best practices can often be presented to staff in a boring memorandum that can easily be ignored. Harry Rosen wanted to create something that motivated and made more of an impact for sales associates.',
			'Watch how we used inexpensive production to convey Rosen-esque style in a series of videos.',
			'The video has become a must-see for all new sales associates and a handy tool to get them juiced about their job.',
			'Let us show you what we can do to sharpen your brand.'
	)
);
