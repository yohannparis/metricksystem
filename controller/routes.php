<?php
/* Router
 * Toro PHP - github.com/anandkunal/ToroPHP
 */
include 'vendor/Toro.php';

/* Handlers */
include 'pages.php';
//include 'project.php';

/* Errors */
ToroHook::add("404",  function() {
	$page = 'page404';
	include 'view/template.php';
});

/* URLs */
Toro::serve(array(

	// Projects
	"/work/([a-zA-Z0-9-]+)" => "projects",

	// E-blasts
	"/(overland)" => "eblast",
	"/(telus)" => "eblast",
	"/(corel)" => "eblast",
	"/(subcretary)" => "eblast",
	"/(harry60)" => "eblast",
	"/(einstein)" => "eblast",
	"/(harrytraining)" => "eblast",

	// Navigation
	"/" => "home",
	"/work" => "work",
	"/clients" => "clients",
	"/what-we-do" => "what_we_do",
	"/about" => "about",
	"/people" => "people",
	"/contact" => "contact",
	"/start-a-project" => "start_a_project",
));
