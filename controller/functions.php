<?php
/*
 * Form Validation
 */

// ---- Cleaning output to avoid JS

function cleanOutput($string)
{
	// In case of the element is a null element or a boolean we do nothing
	if($string == null || is_bool($string)){return $string;}

		$lenght = strlen($string);
		$return="";

		for($i=0; $i<$lenght; ++$i) {
			switch($string[$i]) {
				case "\r": $return.="\\r"; break;
				case "\n": $return.="\\n"; break;
				case "\"":     case "'":     case "<": case ">":  case "&":
					$return .= '\x'.dechex( ord($string[$i] ) );
				break;
				default:
					$return .= $string[$i];
				break;
			}
		}
		return htmlentities(stripslashes(trim($return)), ENT_QUOTES, 'UTF-8');
}


// ---- URL validation

/*
	Syntax
	[scheme] hostname [path] [file[getquery]] [#anchor]
	http://example.com/test/user.php?id=1234#details
 */

function validURL($url)
{

	/* --- Scheme
	 *     http://, https:// or nothing.
	 */
	$regexURL = '((https?)\:\/\/)?';

	/* --- Hostname
	 *     domain and sub-domain with a minimum of 2 characters.
	 *     top-domain 2 or 3 characters.
	 *     example.com, example.ca, example.gov.ca, shop.example.com, test-site.example.com, etc.
	 */
	$regexURL .= '(\w{2,}\.)+[a-zA-Z]{2,4}';

	/* --- Path
	 *     folder path
	 *     /, or /test, /test/, /test/awesome, /test/awesome/
	 */
	$regexURL .= '((\/[\w-]+)*\/?)?';

	 /* --- File
	 *   - Check up the last element of the file if it's a file.
	 *     file.extension
	 *   - GET query
	 *     ?user=123&name=Yohann%20Paris&class=test::cool
	 */
	$regexURL .= '([\w-]+\.\w{2,}(\?[\w+\&\$-][\w;:@\&%=+\$-]*)?)?';

	/* --- Anchor
	 *     #, #title, #cool-Title
	 */
	$regexURL .= '(#[\w-]*)?';

	/* --- Validate the entirely URL */
	$regexURL = '/^'.$regexURL.'$/';

	return preg_match($regexURL, $url);
}


// ---- North America Phone validation

function ValidatePhone($mValue)
{
	$sPattern = '/^[\(]?[0-9]{3}[\)]?[-. ]?[0-9]{3}[-. ]?[0-9]{4}$/';
	return preg_match($sPattern, $mValue);
}


// ---- Canadian Code Postal validation

function ValidatePostal($mValue, $sRegion = '')
{
	$mValue = strtolower($mValue);
	$sFirst = substr($mValue, 0, 1);
	$sRegion = strtolower($sRegion);

	$aRegion = array(
		'nl' => 'a',
		'ns' => 'b',
		'pe' => 'c',
		'nb' => 'e',
		'qc' => array('g', 'h', 'j'),
		'on' => array('k', 'l', 'm', 'n', 'p'),
		'mb' => 'r',
		'sk' => 's',
		'ab' => 't',
		'bc' => 'v',
		'nt' => 'x',
		'nu' => 'x',
		'yt' => 'y'
	);

	if (preg_match('/[abceghjlkmnprstvxy]/', $sFirst) && !preg_match('/[dfioqu]/', $mValue) && preg_match('/^\w\d\w[- ]?\d\w\d$/', $mValue))
	{
		if (!empty($sRegion) && array_key_exists($sRegion, $aRegion))
		{
			if (is_array($aRegion[$sRegion]) && in_array($sFirst, $aRegion[$sRegion]))
			{
				return true;
			}
			else if (is_string($aRegion[$sRegion]) && $sFirst == $aRegion[$sRegion])
			{
				return true;
			}
		}
		else if (empty($sRegion))
		{
			return true;
		}
	}

	return false;
}


/* ===================================================== Email validation ==== */
# RFC 822/2822/5322 Email Parser
# By Cal Henderson <cal@iamcal.com>
#
# This code is dual licensed:
# CC Attribution-ShareAlike 2.5 - http://creativecommons.org/licenses/by-sa/2.5/
# GPLv3 - http://www.gnu.org/copyleft/gpl.html
##################################################################################

function is_valid_email_address($email, $options=array()){

	$defaults = array(
		'allow_comments'	=> true,
		'public_internet'	=> true,
	);

	$opts = array();
	foreach ($defaults as $k => $v) $opts[$k] = isset($options[$k]) ? $options[$k] : $v;
	$options = $opts;
	$no_ws_ctl	= "[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x7f]";
	$alpha		= "[\\x41-\\x5a\\x61-\\x7a]";
	$digit		= "[\\x30-\\x39]";
	$cr		= "\\x0d";
	$lf		= "\\x0a";
	$crlf		= "(?:$cr$lf)";
	$obs_char	= "[\\x00-\\x09\\x0b\\x0c\\x0e-\\x7f]";
	$obs_text	= "(?:$lf*$cr*(?:$obs_char$lf*$cr*)*)";
	$text		= "(?:[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f]|$obs_text)";
	$text		= "(?:$lf*$cr*$obs_char$lf*$cr*)";
	$obs_qp		= "(?:\\x5c[\\x00-\\x7f])";
	$quoted_pair	= "(?:\\x5c$text|$obs_qp)";
	$wsp		= "[\\x20\\x09]";
	$obs_fws	= "(?:$wsp+(?:$crlf$wsp+)*)";
	$fws		= "(?:(?:(?:$wsp*$crlf)?$wsp+)|$obs_fws)";
	$ctext		= "(?:$no_ws_ctl|[\\x21-\\x27\\x2A-\\x5b\\x5d-\\x7e])";
	$ccontent	= "(?:$ctext|$quoted_pair)";
	$comment	= "(?:\\x28(?:$fws?$ccontent)*$fws?\\x29)";
	$cfws		= "(?:(?:$fws?$comment)*(?:$fws?$comment|$fws))";
	$outer_ccontent_dull	= "(?:$fws?$ctext|$quoted_pair)";
	$outer_ccontent_nest	= "(?:$fws?$comment)";
	$outer_comment		= "(?:\\x28$outer_ccontent_dull*(?:$outer_ccontent_nest$outer_ccontent_dull*)+$fws?\\x29)";
	$atext		= "(?:$alpha|$digit|[\\x21\\x23-\\x27\\x2a\\x2b\\x2d\\x2f\\x3d\\x3f\\x5e\\x5f\\x60\\x7b-\\x7e])";
	$atom		= "(?:$cfws?(?:$atext)+$cfws?)";
	$qtext		= "(?:$no_ws_ctl|[\\x21\\x23-\\x5b\\x5d-\\x7e])";
	$qcontent	= "(?:$qtext|$quoted_pair)";
	$quoted_string	= "(?:$cfws?\\x22(?:$fws?$qcontent)*$fws?\\x22$cfws?)";
	$quoted_string	= "(?:$cfws?\\x22(?:$fws?$qcontent)+$fws?\\x22$cfws?)";
	$word		= "(?:$atom|$quoted_string)";
	$obs_local_part	= "(?:$word(?:\\x2e$word)*)";
	$obs_domain	= "(?:$atom(?:\\x2e$atom)*)";
	$dot_atom_text	= "(?:$atext+(?:\\x2e$atext+)*)";
	$dot_atom	= "(?:$cfws?$dot_atom_text$cfws?)";
	$dtext		= "(?:$no_ws_ctl|[\\x21-\\x5a\\x5e-\\x7e])";
	$dcontent	= "(?:$dtext|$quoted_pair)";
	$domain_literal	= "(?:$cfws?\\x5b(?:$fws?$dcontent)*$fws?\\x5d$cfws?)";
	$local_part	= "(($dot_atom)|($quoted_string)|($obs_local_part))";
	$domain		= "(($dot_atom)|($domain_literal)|($obs_domain))";
	$addr_spec	= "$local_part\\x40$domain";

	if (strlen($email) > 254) return 0;

	if ($options['allow_comments']){
		$email = email_strip_comments($outer_comment, $email, "(x)");
	}

	if (!preg_match("!^$addr_spec$!", $email, $m)){
		return 0;
	}

	$bits = array(
		'local'			=> isset($m[1]) ? $m[1] : '',
		'local-atom'		=> isset($m[2]) ? $m[2] : '',
		'local-quoted'		=> isset($m[3]) ? $m[3] : '',
		'local-obs'		=> isset($m[4]) ? $m[4] : '',
		'domain'		=> isset($m[5]) ? $m[5] : '',
		'domain-atom'		=> isset($m[6]) ? $m[6] : '',
		'domain-literal'	=> isset($m[7]) ? $m[7] : '',
		'domain-obs'		=> isset($m[8]) ? $m[8] : '',
	);

	if ($options['allow_comments']){
		$bits['local']	= email_strip_comments($comment, $bits['local']);
		$bits['domain']	= email_strip_comments($comment, $bits['domain']);
	}

	if (strlen($bits['local']) > 64) return 0;
	if (strlen($bits['domain']) > 255) return 0;

	if (strlen($bits['domain-literal'])){

		$Snum			= "(\d{1,3})";
		$IPv4_address_literal	= "$Snum\.$Snum\.$Snum\.$Snum";
		$IPv6_hex		= "(?:[0-9a-fA-F]{1,4})";
		$IPv6_full		= "IPv6\:$IPv6_hex(?:\:$IPv6_hex){7}";
		$IPv6_comp_part		= "(?:$IPv6_hex(?:\:$IPv6_hex){0,7})?";
		$IPv6_comp		= "IPv6\:($IPv6_comp_part\:\:$IPv6_comp_part)";
		$IPv6v4_full		= "IPv6\:$IPv6_hex(?:\:$IPv6_hex){5}\:$IPv4_address_literal";
		$IPv6v4_comp_part	= "$IPv6_hex(?:\:$IPv6_hex){0,5}";
		$IPv6v4_comp		= "IPv6\:((?:$IPv6v4_comp_part)?\:\:(?:$IPv6v4_comp_part\:)?)$IPv4_address_literal";

		if (preg_match("!^\[$IPv4_address_literal\]$!", $bits['domain'], $m)){

			if (intval($m[1]) > 255) return 0;
			if (intval($m[2]) > 255) return 0;
			if (intval($m[3]) > 255) return 0;
			if (intval($m[4]) > 255) return 0;

		}else{

			while (1){

				if (preg_match("!^\[$IPv6_full\]$!", $bits['domain'])){
					break;
				}

				if (preg_match("!^\[$IPv6_comp\]$!", $bits['domain'], $m)){
					list($a, $b) = explode('::', $m[1]);
					$folded = (strlen($a) && strlen($b)) ? "$a:$b" : "$a$b";
					$groups = explode(':', $folded);
					if (count($groups) > 7) return 0;
					break;
				}

				if (preg_match("!^\[$IPv6v4_full\]$!", $bits['domain'], $m)){

					if (intval($m[1]) > 255) return 0;
					if (intval($m[2]) > 255) return 0;
					if (intval($m[3]) > 255) return 0;
					if (intval($m[4]) > 255) return 0;
					break;
				}

				if (preg_match("!^\[$IPv6v4_comp\]$!", $bits['domain'], $m)){
					list($a, $b) = explode('::', $m[1]);
					$b = substr($b, 0, -1); # remove the trailing colon before the IPv4 address
					$folded = (strlen($a) && strlen($b)) ? "$a:$b" : "$a$b";
					$groups = explode(':', $folded);
					if (count($groups) > 5) return 0;
					break;
				}

				return 0;
			}
		}
	}else{

		$labels = explode('.', $bits['domain']);

		if ($options['public_internet']){
			if (count($labels) == 1) return 0;
		}

		foreach ($labels as $label){

			if (strlen($label) > 63) return 0;
			if (substr($label, 0, 1) == '-') return 0;
			if (substr($label, -1) == '-') return 0;
		}

		if ($options['public_internet']){
			if (preg_match('!^[0-9]+$!', array_pop($labels))) return 0;
		}
	}

	return 1;
}

function email_strip_comments($comment, $email, $replace=''){
	while (1){
		$new = preg_replace("!$comment!", $replace, $email);
		if (strlen($new) == strlen($email)){return $email;}
		$email = $new;
	}
}

##################################################################################
