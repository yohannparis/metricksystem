<?php
/*
 *  Pages of simple content
 */

class home {
	function get(){

		// Variables
		$page = 'home';
		$headerWhite = true;
		$footerWhite = true;

		include 'view/template.php';
	}
}

class work {
	function get(){

		// Variables
		$page = 'work';
		$seo_title = 'Work';

		include 'view/template.php';
	}
}

class clients {
	function get(){

		// Variables
		$page = 'clients';
		$seo_title = 'Clients - Work';

		include 'view/template.php';
	}
}

class projects {
	function get($projectTitle){

		// Variables
		$page = 'projects';
		$seo_title = $projectTitle.' - Work';

		include 'view/template.php';
	}
}

class eblast {
	function get($url){

		// Variables
		$page = 'eblast';
		$seo_title = ucwords($url);

		include 'view/template.php';
	}
}

class what_we_do {
	function get(){

		// Variables
		$page = 'what-we-do';
		$seo_title = 'What We Do';
		$headerWhite = true;

		include 'view/template.php';
	}
}

class about {
	function get(){

		// Variables
		$page = 'about';
		$seo_title = 'About';
		$headerWhite = true;

		include 'view/template.php';
	}
}

class people {
	function get(){

		// Variables
		$page = 'people';
		$seo_title = 'People';
		$headerWhite = false;

		include 'view/template.php';
	}
}

class contact {
	function get(){

		// Variables
		$page = 'contact';
		$seo_title = 'Contact';
		$seo_description = "Contact the National Advertising Awards.";

		include 'view/template.php';
	}
}

class start_a_project {
	function get(){

		// Variables
		$sent = false;
		$page = 'start-a-project';
		$seo_title = 'Start a project';

		include 'view/template.php';
	}

	function post(){
		$sent = false;
		// Get the entries and check if they are good
		if(
			isset($_POST['name']) and preg_match("/^[A-Za-z-' .]+$/", $_POST['name']) and
			isset($_POST['email']) and preg_match("/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/", $_POST['email']) and
			isset($_POST['message']) and $_POST['message'] != ''
		){

			// Create the email
			$headers  = "From: " . $_POST['name'] . " <" . $_POST['email'] . ">\n";
			$headers .= "MIME-Version: 1.0\n"; // Define MIME
			$headers .= "Content-Type: text/html; charset=UTF-8\n"; // Define content type and set Boundary

			$body = '<p>'.$_POST['name'].' &lt;'.$_POST['email'].'&gt;:</p>';
			if (isset($_POST['company'])){ $body .= '<p>'.$_POST['company'].'</p>'; }
			$body .= '<p>'.stripslashes(nl2br($_POST['message'])).'</p>';

			if( mail('yohann@metricksystem.com', 'MET.com - Start a project', $body, $headers) ){
				$sent = true;
			}
		}
		// Variables
		$page = 'start-a-project';
		$seo_title = 'Start a project';

		include 'view/template.php';
	}
}
