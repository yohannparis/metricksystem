<?php
	// ---- setup encoding and character set
	header('Content-type: text/html; charset=utf-8');
?><!DOCTYPE html>
<html lang="en-EN">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<link rel="stylesheet" media="all" href="<?=URL;?>view/css/<?=$page;?>.css">
	<?php /* <style type="text/css"><?php include 'css/'.$page.'.css'; ?></style> */ ?>
	<title><?php if(isset($seo_title) and $seo_title !=''){ echo $seo_title.' - '; } ?>The Metrick System - Toronto Marketing Agency</title>
	<link rel="apple-touch-icon-precomposed" href="<?=URL;?>model/images/trailer-iOS.png">
	<link rel="icon" type="image/png" href="<?=URL;?>model/images/favicon.png">

	<?php /*
	<script type="text/javascript">
		var _ss = _ss || [];
		_ss.push(['_setDomain', 'https://koi-16DGM2G.sharpspring.com/net']);
		_ss.push(['_setAccount', 'KOI-1F66XEG']);
		_ss.push(['_trackPageView']);
		(function() {
			var ss = document.createElement('script');
			ss.type = 'text/javascript'; ss.async = true;
			ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'koi-16DGM2G.sharpspring.com/client/ss.js?ver=1.1.1';
			var scr = document.getElementsByTagName('script')[0];
			scr.parentNode.insertBefore(ss, scr);
		})();
	</script>
	*/ ?>

</head>
<body>

	<header <?php if(isset($headerWhite) and $headerWhite){ echo 'class="white"' ; } ?>>

		<nav class="sub-nav">
			<a href="http://trailertrashtalk.ca/">Blog</a>
			<a <?php if(isset($page) and $page == 'start-a-project'){ echo 'class="current"'; } ?> href="<?=URL;?>start-a-project">Start a project</a>
		</nav>

		<nav class="main-nav">
			<a <?php if(isset($page) and ($page == 'work' or $page == 'clients')){ echo 'class="current"'; } ?> href="<?=URL;?>work">Work</a>
			<a <?php if(isset($page) and $page == 'what-we-do'){ echo 'class="current"'; } ?> href="<?=URL;?>what-we-do">What we do</a>
			<a <?php if(isset($page) and $page == 'about'){ echo 'class="current"'; } ?> href="<?=URL;?>about">About</a>
			<a <?php if(isset($page) and $page == 'people'){ echo 'class="current"'; } ?> href="<?=URL;?>people">People</a>
			<a <?php if(isset($page) and $page == 'contact'){ echo 'class="current"'; } ?> href="<?=URL;?>contact">Contact</a>
			<a <?php if(isset($page) and $page == 'start-a-project'){ echo 'class="current"'; } ?> href="<?=URL;?>start-a-project">Start a project</a>
		</nav>

		<a href="<?=URL;?>" class="met-logo"></a>

	</header>

	<?php if(isset($page) and $page == 'what-we-do'){ ?><div class="hero"><img src="<?=URL;?>model/images/whatwedo_main.jpg" alt="what we do"></div><?php } ?>
	<?php if(isset($page) and $page == 'about'){ ?><div class="hero"><img src="<?=URL;?>model/images/about_main.jpg" alt="about"></div><?php } ?>

	<div class="content" id="<?=$page;?>">
		<?php include 'view/pages/'.$page.'.php'; ?>
	</div>

	<footer>
		<div>
			All rights reserved. &copy; <?=date("Y");?> The&nbsp;Metrick&nbsp;System&nbsp;Inc.
			<img src="/model/images/met_trailer_white.png" alt="The Metrick System Inc.">
		</div>
	</footer>

	<script role="mobile navigation">
		<?php // Menu code for Mobile only... Didn't wanted to had server detection for less than 200 bytes ?>
		var mainNav = document.getElementsByClassName('main-nav')[0];
		mainNav.addEventListener('click', function(){
			mainNav.className += ' main-nav-open';
		});
	</script>

<script async src="//cdn.polyfill.io/v1/polyfill.js"></script>
<!--[if lt IE 9]><script async src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lt IE 9]><script async src="//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script><![endif]-->

</body>
</html>
