<nav class="work-nav">
	<a href="/work">Project View</a>
	<a class="active" href="/clients">Clients View</a>
</nav>

<ul>
	<?php
		// list of projects
		include 'model/projects.php';
		shuffle($projects);
		$previousClient = '';
		foreach($projects as $project){
			if(strpos($previousClient, $project['client']) === false){
	?>
				<li>
					<a href="/work/<?=$project['project'];?>">
						<img src="/model/thumbnails/Logo-<?=$project['client'];?>.jpg">
					</a>
				</li>
	<?php
			}
			$previousClient .= $project['client'].", ";
		}
	?>
</ul>
