<?php /*
<nav class="work-nav">
	<a href="javascript:void(0)" class="tag advertising">Advertising</a>
	<a href="javascript:void(0)" class="tag brand-strategy">Brand Strategy</a>
	<a href="javascript:void(0)" class="tag digital">Digital</a>
	<a href="javascript:void(0)" class="tag experiential">Experiential</a>
	<a href="javascript:void(0)" class="tag social-media">Social Media</a>
	<a href="javascript:void(0)" class="tag content-creation">Content Creation</a>
	<a href="javascript:void(0)" class="tag video-production">Video Production</a>
	<a href="javascript:void(0)" class="tag web-design">Web Design</a>
	<a href="javascript:void(0)" class="tag all active">View All</a>
</nav>
*/ ?>

<ul>
	<?php
		// list of projects
		include 'model/projects.php';
		foreach($projects as $project){

			// lowercase and dash the tags
			foreach ($project['tags'] as $value){
			 	$tags[] = str_replace(" ", "-", strtolower($value));
			}
	?>
		<li class="project <?=implode(" ",$tags);?>">
			<a href="/work/<?=$project['project'];?>">
				<img src="/model/thumbnails/Thumbnail-<?=$project['project'];?>.jpg">
				<div class="caption">
					<p class="title"><?=$project['desc'];?></p>
					<p class="tags"><?=implode(", ",$project['tags']);?></p>
				</div>
			</a>
		</li>
	<?php
			$tags = [];
		}
	?>
</ul>

<?php /*
<script>
	var tags = document.getElementsByClassName('tag');	// Get the tags

	for(var i=0; i<tags.length; i++){
		tags[i].addEventListener("click", function(){	// Add a click event on all tags

			// Tag - remove trigger from any previous tag
			for(var j=0; j<tags.length; j++){
				tags[j].className = tags[j].className.replace( /(?:^|\s)active(?!\S)/,'');
			}

			// Project - show all projects
			var projects = document.getElementsByClassName('project');
			for(j=0; j<projects.length; j++){
				projects[j].className = projects[j].className.replace( /(?:^|\s)hidden(?!\S)/,'');
			}

 			// Tag - get which filter tag is used
 			var tag = this.className.split(' ')[1];

 			// If it's to filter them
 			if (tag !== 'all'){

				// Tag - add CSS trigger to selected tag
				this.className += ' active';

				// Project - hide all projects
				for(j=0; j<projects.length; j++){
					projects[j].className = projects[j].className+' hidden';
				}

				// Project - display the project filtered
				projects = document.getElementsByClassName('project '+tag);
				for(j=0; j<projects.length; j++){
					projects[j].className = projects[j].className.replace( /(?:^|\s)hidden(?!\S)/,'');
				}
			}
		});
	}
</script>
*/ ?>
