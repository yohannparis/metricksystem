<h1>YOUR AUDIENCE DOESN’T CARE ABOUT THE PLATFORM, THEY CARE ABOUT YOUR&nbsp;STORY.</h1>

<div class="what-we-do">
	<br><h3>OUR TOOLS</h3>

	<ul>
		<li>
			<img src="/model/capabilities/icon_advertising.png" alt="advertising">
			<h4>ADVERTISING</h4>
			<p>
				We believe in strong creative that touches an emotional chord,
				makes one laugh out loud or smile at its&nbsp;subtlety.
			</p>
		</li>
		<li>
			<img src="/model/capabilities/icon_brandstrategy.png" alt="brandstrategy">
			<h4>BRAND STRATEGY</h4>
			<p>
				After 20 years in the industry we have learned that answering just one question
				&ndash; What <em>should</em> it&nbsp;be? &ndash; can accurately identify your strategic&nbsp;positioning.
			</p>
		</li>
		<li>
			<img src="/model/capabilities/icon_digital.png" alt="digital">
			<h4>DIGITAL</h4>
			<p>
				Digital marketing is often confused with its twin, online marketing. Regardless, if
				you are reading this on your computer, smartphone or tablet &ndash; you&nbsp;get&nbsp;it.
			</p>
		</li>
		<li>
			<img src="/model/capabilities/icon_socialmedia.png" alt="socialmedia">
			<h4>SOCIAL MEDIA</h4>
			<p>
				Social media's main purpose is to provide something for customers to share;
				this creates a referral network with built-in&nbsp;endorsement.
			</p>
		</li>

		<li>
			<img src="/model/capabilities/icon_webdesign.png" alt="webdesign">
			<h4>WEB DESIGN</h4>
			<p>
				A living, breathing digital brochure that is the first place your clients go to determine
				whether you are worthy of their&nbsp;business.
			</p>
		</li>
		<li>
			<img src="/model/capabilities/icon_experiential.png" alt="experiential">
			<h4>EXPERIENTIAL</h4>
			<p>
				Events provide a living, breathing fan experience that will benefit from millions of
				positive impressions, engage customers and drive&nbsp;sales.
			</p>
		</li>
		<li>
			<img src="/model/capabilities/icon_contentcreation.png" alt="content-creation">
			<h4>CONTENT CREATION</h4>
			<p>
				Our goal is to tell stories your clientele is interested in. Will they laugh, will
				they learn something, will they remember it? Success is when they share&nbsp;it.
			</p>
		</li>
		<li>
			<img src="/model/capabilities/icon_videoproduction.png" alt="videoproduction">
			<h4>VIDEO PRODUCTION</h4>
			<p>
				After producing hundreds of videos over 9 years we have learned one thing: Be different.
				Celebrate who you are. Be entertaining. Tell your&nbsp;story.
			</p>
		</li>

	</ul>
</div>

<h3>How we work</h3>

<dl>
	<dt class="active">Your Audience</dt>
	<dd>
		<span>Your Audience</span>
		What motivates your audience, where do they live, how do they find you and what do they care&nbsp;about?
	</dd>

	<dt class="blink">Plot and Structure</dt>
	<dd>
		<span>Plot and Structure</span>
		What should happen here? How should it be? What would be the most satisfying&nbsp;outcome?
	</dd>

	<dt>Your Story</dt>
	<dd>
		<span>Your Story</span>
		Storytelling is one of the most effective tools to share ideas and where we go to make
		abstract concepts meaningful, inspire imagination and motivate&nbsp;action.
	</dd>

	<dt>The Next Chapter</dt>
	<dd>
		<span>The Next Chapter</span>
		Building a world class brand is about taking your audience on a journey; what touch-points
		will turn a prospect into a client and ultimately a friend of your brand, for&nbsp;life.
	</dd>
</dl>

<script>
	var process = document.getElementsByTagName('dt');

	for (index = 0; index < process.length; ++index) {
		process[index].addEventListener('mouseover', function(event){
			process[0].classList.remove('active');
			process[1].classList.remove('blink');
	 	});
	}

	var processlist = document.getElementsByTagName('dl');
	processlist[0].addEventListener('mouseout', function(event){
	 	process[0].classList.add('active');
	 	process[1].classList.add('blink');
	});
</script>
