<p>
	Since 1991, we have created campaigns that put a live person sitting on a toilet
	attached to a billboard, flew the world’s tallest hot air balloon in the shape of
	a teenager and launched Viagra with two words and a&nbsp;smile.
</p>
<p>
	We have been honoured by the Retail Council of Canada, the Billi Awards, Applied Arts Magazine, Retail
	Advertising Club of Chicago, RSVP’s, the Frankies, Hollywood Radio and Television Society Awards,
	the Radio Impact Awards, the New York Festivals Gold Medal, the Web Marketing Awards, the International Broadcast Awards,
	the London International Advertising Awards and Cannes&nbsp;Lions.
</p>

<img class="clients" src="/model/images/clients.png" alt="clients" width="1002">

<div class="office-photos">
	<ul class="office">
		<?php for($i=1;$i<10;$i++){ ?>
			<li class="photo"><img src="/model/office/OfficePhotos-0<?=$i;?>.jpg" alt="OfficePhotos-0<?=$i;?>"></li>
		<?php } ?>
	</ul>
</div>

<div class="recognition">
	<h3>Recognition</h3>
	<?php
		$quotes[] = ['link' => 'http://www.webaward.org/winner.asp?eid=15467#.VEgCOYvF85t', 'text' => 'Winner of the 2010 WebAward for Outstanding Achievement in Web&nbsp;Development.', 'source' => 'Web Marketing Association'];
		$quotes[] = ['link' => 'http://www.canadianinteriors.com/news/go-metrick/1000983934/?&er=NA', 'text' => 'The Metrick System managed to create a space that is bold and environmentally considerate.', 'source' => 'Canadian Interiors'];
		$quotes[] = ['link' => 'http://www.canadianarchitect.com/news/winners-of-the-2012-arido-awards-announced/1001714776/?&er=NA', 'text' => 'Recipient of the 2012  Association of Registered Interior Designers of Ontario (ARIDO) Award of&nbsp;Merit.', 'source' => 'Canadian Architect '];
		$quotes[] = ['link' => 'http://strategyonline.ca/1995/06/12/10686-19950612/', 'text' => 'Special Report: Quick Response&nbsp;Retailing', 'source' => 'Strategy Online'];
		$quotes[] = ['link' => 'http://www.thewellappointedcatwalk.com/2012/12/the-metrick-system-headquarters-by.html', 'text' => 'Google\'s futuristic "napping pods" have got nothing on these vintage Airstream&nbsp;trailers.', 'source' => 'The Well-Appointed Catwalk '];
		$quotes[] = ['link' => 'http://www.marketingmag.ca/brands/mr-sub-delivers-an-education-1940', 'text' => 'Mr.Sub delivers an&nbsp;education', 'source' => 'Marketing Magazine'];
		$quotes[] = ['link' => 'http://www.onextrapixel.com/2013/08/30/taking-a-peek-into-the-worlds-weirdest-offices/', 'text' => 'Number 13 on OXP\'s "Weird Offices Around the World"&nbsp;list', 'source' => 'Onextrapixel'];
		$quotes[] = ['link' => 'http://www.dmnews.com/investment-telemarketing-campaign-provides-call-and-phone/article/80400/', 'text' => 'Investment Telemarketing campaign provides calls… and&nbsp;phones.', 'source' => 'Direct Marketing News'];
	?>
	<ul>
		<?php foreach ($quotes as $quote) { ?>
			<li class="quote">
				<blockquote>
					<a href="<?=$quote['link'];?>" target="_blank">
						"<?=$quote['text'];?>"<cite><?=$quote['source'];?></cite>
					</a>
				</blockquote>
			</li>
		<?php } ?>

		<a href="javascript:void(0)" class="arrow prev">Previous</a>
		<a href="javascript:void(0)" class="arrow next">Next</a>
	</ul>
</div>

<script>
	// ---- Office
		// Put back the photo on display
		function clearDisplay(){
			// Try to get the displayed photo
			var display = document.getElementsByClassName('display');
			// If we got it
			if(display.length > 0){
				// Add a 1/4 second timeout to be smoother
				setTimeout(function(){
					// Remove the display class
					display[0].className = display[0].className.replace( /(?:^|\s)display(?!\S)/,'');
				}, 250);
			}
		}

		// Add an event to every photos
		var photos = document.getElementsByClassName('photo');
		for (var i = photos.length - 1; i >= 0; i--) {
			// When the user click on one of them
			photos[i].addEventListener("click", function(){
				// Remove any previous displayed photos if necessary
				clearDisplay();
				// Display the current photo
				this.className += ' display';
			});

			// When the image is display we can hide it.
			photos[i].children[0].addEventListener("click", function(){
				// Remove any previous displayed photos if necessary
				clearDisplay();
			});
		}

	// ---- Recognition
		var first = document.getElementsByClassName('quote')[0]; // Get the first element of the list
		first.className += ' active'; // Add a marker

		function move(direction) {
			var list = document.getElementsByClassName('quote'); // Get the list of artworks
			var current = document.getElementsByClassName('active')[0]; // Get the current one
			current.className = 'quote'; // Remove is marker

			switch (direction){ // Select what direction we are going

				case 'next': // If we are moving forward
					var index = Array.prototype.slice.call(list).indexOf(current); // Position of the current one
					if (index >= list.length-1){ // But we are at the end of the list
						list[0].className += ' active'; // go back to the beginning
					} else {
						list[index+1].className += ' active'; // keep moving forward
					}
					break;

				case 'prev': // If we are going backward
					var index = Array.prototype.slice.call(list).indexOf(current); // Position of the current one
					if (index < 1){ // if we are at the beggining
						list[list.length-1].className += ' active'; // go to the end
					} else {
						list[index-1].className += ' active'; // keep moving backward
					}
					break;

				default: // Or it's a specific direction
					var index = Array.prototype.slice.call(list).indexOf(direction); // Find the position of the specific one
					list[index].className += ' active'; // display the selected one
					break;
			}
		}

		 // On the click of one of the arrow
		document.getElementsByClassName('arrow prev')[0].addEventListener('click', function() { move('prev');	});
		document.getElementsByClassName('arrow next')[0].addEventListener('click', function() { move('next');	});

		// On the press of one of the keyboard arrow
		document.onkeydown = function(e) {
			e = e || window.event;
			switch (e.keyCode) {
				case 37: // Left key
					move('prev');
					break;
				case 39: // Right key
					move('next');
					break;
			}
		};

		// On the click of one of the pastille
		var list = document.getElementsByClassName('quote'); // Get the list of artworks
		for (var i = list.length - 1; i >= 0; i--) { // Get trough it
			list[i].addEventListener('click', function(){ // Add a click on the event
				move(this); // And move to this specific pastille
			});
		};

		// Automatically run trough the recognitions
		setInterval(function(){ move('next'); }, 5000);
</script>
