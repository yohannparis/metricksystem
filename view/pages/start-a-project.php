<h1>LET’S MAKE SOMETHING<br>GREAT TOGETHER</h1>
<?php if ($sent){ ?>
	<p>
		Thank you for your interest in working with&nbsp;us.<br>
		Your message has been send and we'll get back to you within 48&nbsp;hours.
	</p>
<?php } else { ?>
	<p>
		Thank you for your interest in working with&nbsp;us.<br>
		Just fill in this form and we'll get back to you within 48&nbsp;hours.
	</p>

	<form action="" method="post" novalidate>
		<input type="text" name="name" placeholder="Full Name*"
			<?php if(isset($_POST['name']) and $_POST['name'] != ''){ echo 'value="'.$_POST['name'].'"'; } ?>/>
		<input type="email" name="email" placeholder="Email*"
			<?php if(isset($_POST['email']) and $_POST['email'] != ''){ echo 'value="'.$_POST['email'].'"'; } ?>/>
		<input type="text" name="company" placeholder="Company/Organisation"
			<?php if(isset($_POST['company']) and $_POST['company'] != ''){ echo 'value="'.$_POST['company'].'"'; } ?>/>

		<h2>Anything you want to tell us?</h2>

		<textarea name="message"><?php if(isset($_POST['message']) and $_POST['message'] != ''){ echo $_POST['message']; } ?></textarea>

		<button type="submit">Submit</button>
	</form>
<?php } ?>
