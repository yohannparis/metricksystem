<div class="info">
	<img src="/model/images/contacticon.png" alt="Location" width="56" height="77">

	<a class="address" target="_blank" href="https://goo.gl/maps/5IEwJ">
		<h2>TORONTO</h2>
		100 MIRANDA AVENUE, 2ND FLOOR<br>
		TORONTO, ONTARIO<br>
		M6B 3W7 CANADA
	</a>
	<a class="address" target="_blank" href="https://goo.gl/maps/jfTI0">
		<h2>LOS ANGELES</h2>
		11601 WILSHIRE BLVD., SUITE 500<br>
		LOS ANGELES, CA 90025
	</a>

	<br><span>T.</span> <a href="tel:+14167810151">+1 416 781 0151</a> / +1 800 631 9858
	<br><span>F.</span> +1 416 781 8455

</div>

<div class="contact">
	<div class="project">
		<h1>LET’S MAKE SOMETHING GREAT TOGETHER</h1>
		We’ve prepared a quick questionnaire to get to know you and your project better.
		<a class="button" href="/start-a-project">Start a project</a>
	</div>

	<div class="email">
		<h2>QUESTIONS? ADVICE?</h2>
		We love hearing from you.
		<a class="button" href="mailto:hello@metricksystem.com">SEND US AN EMAIL</a>
	</div>
</div>

