<?php
	include 'model/projects.php';
	$project = $projects[$projectTitle]; // Get the project information
	$video = false; // Trigger to download the Sublime script or not

	// -- For keyboard keys to move in between projects
	$keys = array_keys($projects); // map the project list with their keys
	$current = array_search($projectTitle, $keys); // search which index this project key is
?>
<div class="artworks">

	<?php if(count($project['artwork']) > 1){ ?>
		<ul class="artwork-<?=count($project['artwork']);?>">
			<?php foreach ($project['artwork'] as $index => $artwork) { ?>
				<li class="artwork">
					<?php if($artwork['type'] == 'image') { ?>
						<img src="/model/projects/<?=$artwork['src'];?>" alt="<?=$project['title'].' - '.$index;?>">
					<?php } elseif($artwork['type'] == 'video') { $video = true; ?>
						<video src="<?=$artwork['src'];?>" poster="/model/projects/<?=$artwork['poster'];?>" x-webkit-airplay="allow" preload="none" class="sublime" data-autoresize='fit' width="700" height="400"></video>
					<?php } ?>
				</li>
			<?php } ?>
		</ul>

		<a href="javascript:void(0)" class="arrow prev">Previous</a>
		<a href="javascript:void(0)" class="arrow next">Next</a>

		<?php if($video){ ?><script src="//cdn.sublimevideo.net/js/plbikrio.js"></script><?php } ?>
		<script>
				var first = document.getElementsByClassName('artwork')[0]; // Get the first element of the list
				first.className += ' active'; // Add a marker

				function move(direction) {
					var list = document.getElementsByClassName('artwork'); // Get the list of artworks
					var current = document.getElementsByClassName('active')[0]; // Get the current one
					current.className = current.className.replace( /(?:^|\s)active(?!\S)/,''); // Remove is marker

					switch (direction){ // Select what direction we are going
						case 'next': // If we are moving forward
							var index = Array.prototype.slice.call(list).indexOf(current); // Position of the current one
							if (index >= list.length-1){ // But we are at the end of the list
								list[0].className += ' active'; // go back to the beginning
							} else {
								list[index+1].className += ' active'; // keep moving forward
							}
							break;

						case 'prev':// Else we are going backward
							var index = Array.prototype.slice.call(list).indexOf(current); // Position of the current one
							if (index < 1){ // if we are at the beggining
								list[list.length-1].className += ' active'; // go to the end
							} else {
								list[index-1].className += ' active'; // keep moving backward
							}
							break;

						default: // Or it's a specific direction
							var index = Array.prototype.slice.call(list).indexOf(direction); // Find the position of the specific one
							list[index].className += ' active'; // display the selected one
							break;
					}
				}

				// On the click of one of the arrow
				document.getElementsByClassName('arrow prev')[0].addEventListener('click', function() { move('prev');	});
				document.getElementsByClassName('arrow next')[0].addEventListener('click', function() { move('next');	});

				// On the click of one of the pastille
				var list = document.getElementsByClassName('artwork'); // Get the list of artworks
				for (var i = list.length - 1; i >= 0; i--) { // Get trough it
					list[i].addEventListener('click', function(){ // Add a click on the event
						move(this); // And move to this specific pastille
					});
				};

				// On the press of one of the keyboard arrow
				document.onkeydown = function(e) {
					e = e || window.event;
					switch (e.keyCode) {
						case 37: // Left key
							move('prev');
							break;
						case 38: // Up key
							window.location.href = '/work/<?=$keys[$current-1];?>';
							break;
						case 39: // Right key
							move('next');
							break;
						case 40: // Bottom key
							window.location.href = '/work/<?=$keys[$current+1];?>';
							break;
					}
				};
		</script>

	<?php } else { ?>

		<?php if($project['artwork'][0]['type'] == 'image') { ?>
			<img src="/model/projects/<?=$project['artwork'][0]['src'];?>" alt="<?=$project['title'].$index;?>">
		<?php } elseif($project['artwork'][0]['type'] == 'video') { $video = true; ?>
			<script src="//cdn.sublimevideo.net/js/plbikrio.js"></script>
			<?php //<script src="/view/js/plbikrio.js"></script> ?>
			<?php //<script src="/view/js/sa.js"></script> ?>
			<video src="<?=$project['artwork'][0]['src'];?>" poster="/model/projects/<?=$project['artwork'][0]['poster'];?>" x-webkit-airplay="allow" preload="none" class="sublime" data-autoresize='fill' width="700" height="400"></video>
		<?php } ?>

		<script>
			// On the press of one of the keyboard arrow
			document.onkeydown = function(e) {
				e = e || window.event;
				switch (e.keyCode) {
					case 38: // Up key
							window.location.href = '/work/<?=$keys[$current-1];?>';
						break;
					case 40: // Bottom key
							window.location.href = '/work/<?=$keys[$current+1];?>';
						break;
				}
			};
		</script>

	<?php } ?>

</div>

<div class="info">
	<h1><?=$project['title'];?></h1>
	<p><?=$project['texte'];?></p>
</div>

<aside>
	<?php if($project['client'] !== ''){ ?>
		<div>
			<span>Client</span>
			<img src="/model/clients/<?=$project['client'];?>">
		</div>
	<?php } ?>
	<div>
		<span>Services</span>
		<?=implode("<br>",$project['tags']);?>
	</div>
</aside>
