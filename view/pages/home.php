<script src="/vendor/jquery-2.1.1.min.js"></script>
<script src="/vendor/jquery.vide.min.js"></script>
<script>
	$(document).ready( function (){
		$("#home").vide({
			mp4: "/model/video/video",
			webm: "/model/video/video",
			poster: "/model/video/video"
		}, {
			muted: false,
			loop: false,
			autoplay: true,
			volume: .2,
			position: "50% 50%", // Similar to the CSS `background-position` property.
			posterType: "jpg" // Poster image type. "detect" — auto-detection; "none" — no poster; "jpg", "png", "gif",... - extensions.
		});

		// Trigger to pause the video
		function playPause(video) {
			if(video.paused){
				video.play();
			} else {
				video.pause();
			}
		}

		// If somebody click on the video we pause it.
		var video = document.getElementsByTagName('video')[0];
		video.addEventListener('click', function(){
			playPause(video);
		});

		// Or if we press the space bar, we also pause the video
		document.onkeydown = function(e) {
			e = e || window.event;
			if(e.keyCode === 32) {
				playPause(video);
			}
		};
	});
</script>
