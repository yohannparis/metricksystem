<?php
	include 'model/eblast/eblasts.php';

	$eblast = $eblasts[$url];
?>

<img class="eblast-header" src="/model/eblast/<?=$eblast['header'];?>">

<div class="eblast-content">

	<p><?=$eblast['paragraph'][0];?></p>
	<p><?=$eblast['paragraph'][1];?></p>

	<video x-webkit-airplay="allow" controls>
		<source src="<?=$eblast['video']['webm'];?>" type="video/webm">
		<source src="<?=$eblast['video']['mp4'];?>" type="video/mp4">
	</video>

	<p><?=$eblast['paragraph'][2];?></p>
	<p><?=$eblast['paragraph'][3];?></p>

	<a class="button" href="/start-a-project">Start a Project</a>

</div>

<script src="//cdn.sublimevideo.net/js/plbikrio.js"></script>
