<?php

$employees[] = array(
	"name" => "LAURENCE METRICK",
	"title" => "Principal & Creative Director",
	"bio" => "His name is different. His office is different. His approach is different. And it’s what makes his clients different from everybody else at the end of the day. After completing his undergrad in Canada and post-graduate degree at Université de la Sorbonne in France, Laurence returned home to begin his career in advertising, eventually founding The Metrick System back in 1991. Since then he has led the company’s success in a broad range of skills including retail marketing, design, advertising, production, digital, social and experiential projects. When he’s not deepening his knowledge of effectively communicating a brand’s story, he’s swimming a mile and collecting Air Stream trailers from the 60’s. Like we said, Laurence is anything but conventional.",
	"image" => "staffbios-06.jpg"
);

$employees[] = array(
	"name" => "CAROLYN MEJIA",
	"title" => "Planning & Strategy",
	"bio" => "Carolyn has had her conative strengths scientifically tested. As it turns out she is a 7 Fact Finder. It means that in order to make a decision she must first ask every conceivable question before deciding to go a certain direction. This personality trait makes her perfect as lead Planner and Strategist for client communications here at The Metrick System — exploring countless avenues to arrive at the best path for success. Her Masters Degree in Corporate Communication from Rutgers doesn’t hurt either. Just don’t ask her what toppings she wants on her pizza. You may have to clear your schedule.",
	"image" => "staffbios-07.jpg"
);

$employees[] = array(
	"name" => "ELLIE METRICK",
	"title" => "Marketing & Communication Manager",
	"bio" => "So what’s a trained chef with a Masters Degree in Art Business doing in the storytelling biz? Satisfying the relentless appetites of clients in the most artful way possible, of course. It’s that ability to balance the needs of suppliers, teammates and others that has been Ellie’s recipe for success. On any given day, she can be found responding to untimely requests, delivering upon impossible timelines, or managing multi-faceted projects like the building of the world’s first Einstein Museum. And all with a smile on her face. Hmmm, maybe it has something to do with knowing the best restaurants to eat at for all those demanding client dinners?",
	"image" => "staffbios-08.jpg"
);

$employees[] = array(
	"name" => "ROBIN HEISEY",
	"title" => "Creative Director",
	"bio" => "If you have a sneaking suspicion that you’ve seen Robin before your instincts wouldn’t be wrong. He’s been at the forefront of brand storytelling for the past 30 years, working as CCO for FCB, VP Creative Director and Partner at Gee Jeffrey, and as a seasoned writer for a list of notable agencies in Toronto. It’s a discipline he employs daily at work and shares openly in his spare time via his blog. Along with his busy work life he also teaches storytelling at The School of Media Studies, Humber College. When not at The Metrick System guiding the artful ways in which clients tell their story, he’s pursuing a host of other passions including his keen interest as an audiophile. Proof of one of Robin’s best qualities as a CD; being a good listener.",
	"image" => "staffbios-09.jpg"
);

$employees[] = array(
	"name" => "MARINA LUONG",
	"title" => "Art Director & Designer",
	"bio" => "When she’s not applying her deep experience in print and digital media or overseeing complex visual programs for clients, Marina can be found fuelling her other passion. Namely, spiking a volleyball with the same white-hot intensity she brings to her design skills. Clients rely on her to serve up an ace for their brand’s visual identity across all mediums. It’s that ability to recognize how elements can come together to communicate a message that makes her such a valued team member. Kinda like how a one-two-two coverage can say, “don’t test this “D”.",
	"image" => "staffbios-10.jpg"
);

$employees[] = array(
	"name" => "STEVE GARDNER",
	"title" => "Writer Director",
	"bio" => "Steve is a wine lover, a fight fan and he practices Tai Chi. Which may not make sense on the surface, but it does kinda speak to his vocation. Steve has worked for agencies in New York and Toronto and has been part of the advertising scene for over 20 years. At The Metrick System he is challenged daily to write about many different subjects, simultaneously. When he’s not drafting copy for a website, a script for an online video, headlines for print and billboards or posts for digital media, he’s directing actors in the studio attempting to pull from them their best performance. When they don’t, he simply falls back on those other passions.  Namely, finding the Zen, having a glass of wine and learning to fight another day.",
	"image" => "staffbios-14.jpg"
);

$employees[] = array(
	"name" => "YASMINE LAASRAOUI",
	"title" => "Account Coordinator",
	"bio" => "We’re not saying she’s over-qualified for her job. Even though you could say it in four different languages and she could understand every word. Yasmine is The Metrick Systems local cat-herder, keeping everyone in line project-wise. She’s the one reminding us what needs to get done on what day. When she’s not leveraging her strong communication skills to make tough deadlines, she’s following her other passion as a self-proclaimed music junkie. Which makes sense, cause when things get done on time it’s like music to a client’s ears.",
	"image" => "staffbios-15.jpg"
);

$employees[] = array(
	"name" => "KUN ZHANG",
	"title" => "Project Coordinator",
	"bio" => "Kun tackles everything she does with extreme focus. As head of our client research department, it’s not uncommon to find her head down, analyzing endless amounts of data in order to help clients discover new opportunities. Kinda like when she’s intensely focussed on finding a toe hook in order to reach the summit of a weekend rock climb. It’s this discipline that led to her Masters in Sociology from Western University, and probably the reason why Kun isn’t easily distracted. Well, unless you happen to have a super fresh sashimi roll.",
	"image" => "staffbios-17.jpg"
);

$employees[] = array(
	"name" => "SUJITHA GANESHAN",
	"title" => "Accountant",
	"bio" => "When she’s not issuing cheques, reconciling budgets or filing tax returns, you’ll find The Metrick System’s general accountant cheering on her favourite Toronto sports team. In many ways, Sujitha has applied her love of sport to her job, keeping us out of the CRA’s penalty box and billing faster than a Roger Clemens fastball. But if you really want to know why we love Sujitha, swing by around lunch hour and you may get a taste of some or her amazing Srilankan cooking. Talk about a major score.",
	"image" => "staffbios-16.jpg"
);

$employees[] = array(
	"name" => "CHRIS MORRIS",
	"title" => "Video Editor & Animation",
	"bio" => "Chris is used to being busy. As a video editor and master of all things AV, he is continually tasked with creating impactful videos to help brands tell their story. He’s been doing this for close to 25 years, working for companies like Telus, Mr. Sub, Harry Rosen and Hebrew University, facilitating projects from inception to final delivery. So how does someone this busy have time to do all this and raise three children, beekeeping and take boxing? We haven’t a clue.",
	"image" => "staffbios-12.jpg"
);

$employees[] = array(
	"name" => "SIMON KIM",
	"title" => "Illustrator & Designer",
	"bio" => "Simon watches people. No, not like that. He observes them and picks up in finite detail, the symmetry of their eyes, variance of colour in their hair and the subtle balance of their physical qualities. In short, he is passionate about everything he sees. As an illustrator and designer, he has created compelling and dynamic visual projects for The National Post, Elte and Society 6, and his work has been recognized by Applied Arts Magazine, Creative Quarterly and Digital Arts. As for his spare time? You guessed it. He sketches people on the street.",
	"image" => "staffbios-13.jpg"
);

$employees[] = array(
	"name" => "YOHANN PARIS",
	"title" => "Web Developer",
	"bio" => "You know the french term “laissez-faire”? You won’t find it applies to Yohann; web developer and resident frenchman at The Metrick System. More often than not you’ll find him, ear buds on, intensely focused on programming a client’s website or new Internet-based application. His Masters degree in Management & Information Systems from Université de Savoie in France serves him well, creating full-stack web development. Those French roots also happen to serve him well when selecting the perfect complement to a cave aged Gruyere. Gotta love the French.",
	"image" => "staffbios-11.jpg"
);

?>

<ul>
	<?php foreach ($employees as $employee) { ?>
		<li class="employee">
			<img src="/model/staff/<?=$employee['image'];?>" alt="<?=$employee['name'];?>">
			<div class="caption">
				<h2><?=$employee['name'];?><span><?=$employee['title'];?></span></h2>
				<p><?=$employee['bio'];?></p>
			</div>
		</li>
	<?php } ?>
</ul>

<h3>Want to join the&nbsp;team?</h3>
<p>email us at <a href="mailto:hello@metricksystem.com">hello@metricksystem.com</a><br>and tell us about&nbsp;yourself</p>
