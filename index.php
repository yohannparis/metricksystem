<?php

/*
 * Global Constants
 */

// ---- display no errors online
error_reporting(0);

// ---- Set include path
$path = '/Users/admin/Development/metricksystem.com/';
set_include_path(get_include_path() . PATH_SEPARATOR . $path);

// ---- Date
date_default_timezone_set('America/Toronto');
$date = new dateTime('now');
define('DATE', $date->format('Y-m-d H:i'));

// ---- SEO info
define('SEODESC',"We produce engaging, emotionally-driven, entertaining work with one purpose: to sell what you do. We're a small, but mighty band of creative marketing zealots.");

// ---- Online server or not
define('URL', '/');

/*
 * Sessions and Router
 */

ob_start('ob_gzhandler'); // Compress output and turn on buffer

require_once 'controller/functions.php';
require_once 'controller/routes.php';

ob_end_flush(); // Send the Output Buffering
